<?php

namespace App;

use Redis;
use Illuminate\Database\Eloquent\Model;

class Order
{
    public static function new()
    {
        return collect([
            [
                'end'       => 'Mangalam  Paradise,  Rohini  Sec  3,  New  Delhi',
                'coordinates'   => [
                    'lat'   => '28.698208',
                    'lng'   => '77.114662'
                ] 
            ],
            [
                'end'       => 'Qutub  Minar,  Mehrauli,  New  Delhi',
                'coordinates'   => [
                    'lat'   => '28.524428',
                    'lng'   => '77.185456'
                ] 
            ],
            [
                'end'       => 'Connaught  Place,  New  Delhi',
                'coordinates'   => [
                    'lat'   => '28.631451',
                    'lng'   => '77.216667'
                ] 
            ],
            [
                'end'       => 'Ajmal  Khan  Road,  Karol  Bagh,  New  Delhi',
                'coordinates'   => [
                    'lat'   => '28.650334',
                    'lng'   => '77.192316'
                ] 
            ],
            [
                'end'       => 'Cyber  Hub,  Gurgaon,  Haryana',
                'coordinates'   => [
                    'lat'   => '28.495218',
                    'lng'   => '77.089134'
                ] 
            ]
        ])->random(3);
    }

    public static function update($id, $data)
    {
        Redis::hmset('orders:' . $id, $data);
    }

    public static function get($id)
    {
        return Redis::hgetall('orders:' . $id);
    }
}

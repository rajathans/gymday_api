<?php

namespace App\Http\Controllers;

use Redis;
use Illuminate\Http\Request;

use App\Order;

class OrderController extends Controller
{
    /**
     * Get details of a single order
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::get($id);
        
        return response([
            'data' => [
                'order'         => $order,
                'checkpoints'   => json_decode($order['checkpoints'], 1)
            ]
        ]);
    }

    /**
     * Add a new checkpoint for an order
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function addCheckpoint($id, Request $request)
    {
        $order = Order::get($id);

        if (empty($order)) {
            return response(['message' => 'Order not found', 'data' => []], 400);
        }

        if (empty($request->lat) || empty($request->lng)) {
            return response(['message' => 'Invalid lat/lng', 'data' => []], 400);
        }

        $checkpoints = json_decode($order['checkpoints'], 1);
        array_push($checkpoints, ['lat' => $request->lat, 'lng' => $request->lng]);

        if (count($checkpoints) == 1) {
            $data = [
                'checkpoints'   => json_encode($checkpoints),
                'startLat'      => $request->lat,
                'startLng'      => $request->lng
            ];
        } else {
            $data = [
                'checkpoints' => json_encode($checkpoints)
            ];
        }

        Order::update($id, $data);

        return response(['data' => $checkpoints]);
    }

    /**
     * Update the value of ETA for an order
     *
     * @param int $id
     * @param Request $request
     * @return void
     */
    public function updateEta($id, Request $request)
    {
        $order = Order::get($id);

        if (empty($order)) {
            return response(['message' => 'Order not found', 'data' => []], 400);
        }

        if (empty($request->eta)) {
            return response(['message' => 'Invalid ETA', 'data' => []], 400);
        }

        Order::update($id, [
            'eta' => $request->eta
        ]);

        return response([]);
    }

    /**
     * Update status of order
     *
     * @param int $id
     * @return void
     */
    public function finishOrder($id) {
        $order = Order::get($id);

        if (empty($order)) {
            return response(['message' => 'Order not found', 'data' => []], 400);
        }

        Order::update($id, [
            'isActive' => 'no',
            'status' => 'FINISHED_DELIVERY'
        ]);

        if (!empty(Order::get($id+1))) {
            Order::update($id + 1, [
                'isActive' => 'yes',
                'status'   => 'ACTIVE_DELIVERY'
            ]);
        }

        return response([]);
    }
}

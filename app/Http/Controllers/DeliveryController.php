<?php

namespace App\Http\Controllers;

use Redis;
use Illuminate\Http\Request;

use App\{DeliveryList, Order};

class DeliveryController extends Controller
{
    /**
     * Get list of all orders
     *
     * @return void
     */
    public function index()
    {   
        $data = [];
        for ($i=1; $i<=3; $i++) {
            $data[] = Order::get($i);
        }

        return response(['data' => $data]);
    }

    /**
     * Create a new list of orders
     *
     * @return void
     */
    public function store()
    {
        Redis::flushDB();
        $orders     = Order::new();
        $response   = [];
        
        for ($i = 1; $i <= count($orders); $i++) {
            $data = [
                'id'                => $i,
                'endLocation'       => $orders[$i-1]['end'],
                'startLocation'     => '',
                'endLat'            => $orders[$i-1]['coordinates']['lat'],
                'endLng'            => $orders[$i-1]['coordinates']['lng'],
                'startLat'          => '',
                'startLng'          => '',
                'eta'               => '',
                'status'            => $i == 1 ? 'ACTIVE_DELIVERY' : 'ON_DELIVERY',
                'checkpoints'       => json_encode([]),
                'isActive'          => $i == 1 ? 'yes' : 'no'
            ];

            Order::update($i, $data);

            $response[] = $data;
        }

        return response(['data' => $response]);
    }
}

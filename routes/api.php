<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'lists'], function () {
    Route::get('/', 'DeliveryController@index');
    Route::post('/', 'DeliveryController@store');
});

Route::group(['prefix' => 'orders'], function () {
    Route::get('{id}', 'OrderController@show');
    Route::put('{id}/checkpoint', 'OrderController@addCheckpoint');
    Route::put('{id}/eta', 'OrderController@updateEta');
    Route::put('{id}/finish', 'OrderController@finishOrder');
});